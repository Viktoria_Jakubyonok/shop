﻿using Microsoft.AspNetCore.Mvc;
using Shop.Application.OrdersAdmin;
using Shop.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.UI.Controllers
{
    [Route("[controller]")]
    public class OrderControllers: Controller
    {

        private ApplicationDbContext _ctx;

        public OrderControllers(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }
        [HttpGet("")]
        public ActionResult GetOrders(int status) => Ok(new GetOrders(_ctx).Do(status));
        [HttpGet("{id}")]
        public ActionResult GetOrder(int id) => Ok(new GetOrder(_ctx).Do(id));
        [HttpGet("{id}")] 
        public async Task<ActionResult> UpdateOrder(int id) => Ok((await new UpdateOrder(_ctx).Do(id)));
    }
}
