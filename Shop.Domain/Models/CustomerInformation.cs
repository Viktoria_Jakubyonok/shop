﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop.Domain.Models
{
   public class CustomerInformation
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public string AddressOne { get; set; }
        public string AddressTwo { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
    }
}
